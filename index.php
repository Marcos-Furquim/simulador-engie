<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <meta charset="UTF-8">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <title>ENGIE Solar | Minha Energia Solar</title>
    <link rel="icon" href="http://www.engie.com/favicon.png" />
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/foundation.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/fotorama.css">
    <link rel="stylesheet" type="text/css" href="assets/css/components/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/components/owl-slider2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/components/owl.theme.default2.min.css">
</head>


<body>
    <!-- global -->
    <div class="global simulator-dot">


        <!-- header -->
        <section class="header">
            <div class="tittle-wrapper scroll">
                <div class="logo-wrapper">
                    <img src="./assets/images/files/logo.png" />
                </div>
                <h1 class="title"><b>Quanto você quer economizar</b><br/> na conta de energia elétrica?</h1>
                <p>Ao gerar energia solar para abastecer sua casa ou empresa é possível economizar até 90% na conta de energia elétrica. A ENGIE é referência nacional nesse tipo de solução, além de ser a maior geradora privada de energia no mundo.</p>
                <span class="know-more">Saiba Mais</span>
            </div>
            <div class="slider-wrapper scroll">
                <h2><b>Por que investir em energia solar?</b></h2>
                <div class="slider-investimento-solar owl-carousel owl-theme">
                    <div class="item">
                        <div class="vertical-center">
                            <img src="assets/images/icons/cash.svg" />
                            <p class="title">Economia</p>
                            <p class="description">Tenha previsibilidade na sua conta de luz e fuja dos reajustes frequentes das concessionárias de energia.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="vertical-center">
                            <img src="assets/images/icons/payback.svg" />
                            <p class="title">Payback</p>
                            <p class="description">A instalação do sistema fotovoltaico paga-se com a economia na conta de luz.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="vertical-center">
                            <img src="assets/images/icons/lowcost.svg" />
                            <p class="title">Baixo custo de manutenção</p>
                            <p class="description">A principal manutenção periódica é a limpeza dos equipamentos.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="vertical-center">
                            <img src="assets/images/icons/renewable-source.svg" />
                            <p class="title">Fonte renovável e infinita</p>
                            <p class="description">O Brasil tem um dos melhores recursos solares do mundo, pois recebe incidência solar superior a 2000 horas por ano.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="vertical-center">
                            <img src="assets/images/icons/quick-installation.svg" />
                            <p class="title">Instalação rápida</p>
                            <p class="description">Em  média, em apenas três meses você já começa a aproveitar os benefícios da energia solar, sem se preocupar com obras ou reformas.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--simulator box-->
        <section class="simulator">
            <h2>Descubra <b>quanto você pode</b> economizar</h2>
            <p>Faça uma simulação e veja quanto você vai poupar já no primeiro ano de operação </p>

            <div class="box-simulator">
                <form id="simulator-form">
                    <div class='form-items'>
                        <div class="form-item form-item-float-label">
                            <select name="simulador-estado" id="simulador-estado" class="form-select" required>
                                <option disabled val=""></option>
                            </select>
                            <label for="simulador-estado" class="form-label">Estado</label>
                        </div>
                        <div class="form-item form-item-float-label">
                            <select name="simulador-cidade" id="simulador-cidade" class="form-select" required>
                                <option disabled val=""></option>
                            </select>
                            <label for="simulador-cidade" class="form-label">Cidade</label>
                            <input type='hidden' name="distribuidora" id="lista_distribuidoras" class="distribuidora_cidade" data-parsley-trigger="change">
                        </div>
                        <div class="form-item">
                            <select name="simulador-tipo-pessoa" id="simulador-tipo-pessoa" class="form-select">
                                <option value="1" selected>Pessoa Física</option>
                                <option value="2">Pessoa Jurídica</option>
                            </select>
                        </div>
                        <div class="form-item form-item-float-label form-item-valor-mensal">
                           <input name="simulador-valor-mensal" id="simulador-valor-mensal" type="text" class="form-text money" required >
                           <label for="simulador-valor-mensal" class="form-label">Valor mensal de energia</label>
                           <div class="radio-content"> 
                               <input type="radio" name="simulador-unidade" id="simulador-unidade-rs" value="R$" checked="checked">
                               <label for="simulador-unidade-rs">R$</label> 
                               <input type="radio" name="simulador-unidade" id="simulador-unidade-kwh" value="kWh">
                               <label for="simulador-unidade-kwh">kWh</label>
                           </div>
                        </div>
                    </div>
                    <div class="btn-wrapper">
                        <button type="submit" class="btn" id="btnSimular">Calcular!</button>
                    </div>
                </form>
            </div>

        </section>
        <div id="loading">
            <img src="assets/images/icons/loading.svg" width="200">
            <h2 class="blue margin-top-20">Simulando</h4>
        </div>
        <div id="result-error">
            <h2>Infelizmente seu consumo é muito baixo para que um sistema solar fotovoltaico possa ser viável. Agradecemos seu interesse!</h2>
        </div>
        <!--result-->
        <section class="result">
            <h2><b>Resultado</b> da sua simulação:</h2>
            <p>Confira abaixo algumas informações sobre seu sistema.</p>
            <div class="result-cards">
                <div class="result-card">
                    <div class="svg-wrapper">
                        <svg class="meter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 122.473 66.237">
                            <g transform="translate(1.3 1.3)">
                            <g class="group-arrow-point">
                                <path class="arrow-point" d="M38.573,2.9,7.8,19.192l4.526,5.431,4.526,4.526Z"/>
                            </g>
                            <g transform="translate(0.2 0.2)">
                                <path class="circle" d="M1.105,64.462c0-2.715-.905-4.526-.905-7.241C.2,25.543,27.353.2,59.937.2s59.737,25.343,59.737,57.021a19.929,19.929,0,0,1-.905,7.241" transform="translate(-0.2 -0.2)"/>
                            </g>
                            <g transform="translate(40.024 37.309)">
                                <path class="arrow-base" d="M46.235,24.212C46.235,13.351,37.184,4.3,25.417,4.3S4.6,13.351,4.6,24.212c0,2.715.905,4.526,1.81,7.241H44.424a17.051,17.051,0,0,0,1.81-7.241" transform="translate(-4.6 -4.3)"/>
                            </g>
                            </g>
                        </svg>
                    </div>
                    <div class="result-description">
                        <span class="text-result">Sua cidade é</span>
                        <span id="city-classification" class="output-result">Regular</span>
                        <span class="text-result">para energia solar!</span>
                    </div>                    
                </div>
                <div class="result-card" id="card-placa">
                    <div class="svg-wrapper">
                        <svg class="sun-placa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 162.406 162.406">
                            <g>
                                <g>
                                <path class="cls-1" d="M149.782,82.854a1.2,1.2,0,0,0,1.185-1.185V70.685a1.185,1.185,0,0,0-2.369,0V81.67a1.161,1.161,0,0,0,1.185,1.185" transform="translate(-68.579 -32.076)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M132.53,82.052a1.244,1.244,0,0,0-.862,1.508l1.4,5.223a1.261,1.261,0,0,0,1.185.915.683.683,0,0,0,.323-.054,1.244,1.244,0,0,0,.862-1.508l-1.4-5.223a1.2,1.2,0,0,0-1.508-.862" transform="translate(-60.75 -37.843)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M164.268,82.914l-1.4,5.223a1.27,1.27,0,0,0,.862,1.508.683.683,0,0,0,.323.054,1.261,1.261,0,0,0,1.185-.915l1.4-5.223a1.27,1.27,0,0,0-.862-1.508,1.2,1.2,0,0,0-1.508.862" transform="translate(-75.149 -37.843)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M104.058,101.743a1.071,1.071,0,0,0,.862-.377,1.2,1.2,0,0,0,0-1.723l-7.808-7.754a1.218,1.218,0,0,0-1.723,1.723l7.754,7.754a1.261,1.261,0,0,0,.915.377" transform="translate(-43.856 -42.241)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M97.968,117.514a1.31,1.31,0,0,0,1.077-.592,1.218,1.218,0,0,0-.431-1.669l-4.685-2.692a1.219,1.219,0,0,0-1.239,2.1l4.685,2.692a1.227,1.227,0,0,0,.592.162" transform="translate(-42.504 -51.873)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M116.591,88.67a1.218,1.218,0,0,0-.431,1.669l2.692,4.685a1.22,1.22,0,0,0,1.023.592,1.086,1.086,0,0,0,.592-.162,1.218,1.218,0,0,0,.431-1.669L118.206,89.1a1.216,1.216,0,0,0-1.615-.431" transform="translate(-53.534 -40.853)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M203.053,131.891a.683.683,0,0,0,.323-.054l5.223-1.4a1.228,1.228,0,0,0-.646-2.369l-5.223,1.4a1.244,1.244,0,0,0-.862,1.508,1.2,1.2,0,0,0,1.185.915" transform="translate(-93.149 -59.089)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M87.076,128.168a1.228,1.228,0,1,0-.646,2.369l5.223,1.4a.683.683,0,0,0,.323.054,1.234,1.234,0,0,0,.323-2.423Z" transform="translate(-39.474 -59.135)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M197.366,91.888a1.2,1.2,0,0,0-1.723,0l-7.754,7.754a1.2,1.2,0,0,0,0,1.723,1.271,1.271,0,0,0,.862.377,1.071,1.071,0,0,0,.862-.377l7.754-7.754a1.2,1.2,0,0,0,0-1.723" transform="translate(-86.546 -42.241)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M179.052,89.1l-2.692,4.685a1.208,1.208,0,0,0,1.023,1.831,1.154,1.154,0,0,0,1.023-.592l2.692-4.685a1.2,1.2,0,1,0-2.046-1.239" transform="translate(-81.318 -40.853)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M197.537,117.514a1.086,1.086,0,0,0,.592-.162l4.685-2.692a1.219,1.219,0,1,0-1.239-2.1l-4.685,2.692a1.218,1.218,0,0,0-.431,1.669,1.242,1.242,0,0,0,1.077.592" transform="translate(-90.594 -51.873)"/>
                                </g>
                                <g>
                                <path class="cls-1" d="M100.14,161.871v8.777H90.178a1.185,1.185,0,1,0,0,2.369h27.839a1.185,1.185,0,0,0,0-2.369h-9.854v-8.777h49.109a1.309,1.309,0,0,0,.969-.485,1.268,1.268,0,0,0,.162-1.077l-11.47-33.44a1.215,1.215,0,0,0-1.131-.808h-18.47a23.193,23.193,0,0,0-46.309,0H62.608a1.164,1.164,0,0,0-1.131.808L49.954,160.31a1.268,1.268,0,0,0,.162,1.077,1.151,1.151,0,0,0,.969.485H100.14Zm-21-2.423,2.315-13.408,4.308-2.908H99.44l3.662,2.8V159.5H79.14Zm22.67-17.5h0l1.239-.862.485-.377.7-.538.754.538.485.377,1.185.862h.054l-1.292.915-.323.269-.862.646-.862-.646-.323-.269Zm3.985,28.755h-3.231v-8.777h3.231Zm1.185-11.254h-1.562v-13.57l3.662-2.8h13.677l4.308,2.908,2.315,13.408h-22.4Zm48.679,0H131.8l-2.369-13.677,3.016-2.639H150Zm-28.216-31.017H145.05l4.2,12.277H132.4l-4.254-2.746-1.077-6.3-.538-3.231Zm-.969,12.277.646-.538.215.108.646.377.754.485,1.131.7.215.162-1.023.915-.162.162-.754.646-.162-.108-.808-.538-.592-.431-1.185-.808-.162-.108,1.023-.862Zm-.754-2.531-1.831,1.508-1.185.969H109.079l-3.662-2.639v-9.639h18.578l.538,3.231.862,5.116Zm-21.485-31.609a20.831,20.831,0,0,1,20.732,19.439H83.5a20.831,20.831,0,0,1,20.732-19.439M83.017,136.778l.862-5.116.538-3.231h18.578v9.639l-3.662,2.639H85.709l-1.185-.969-1.831-1.508Zm-.754,4.2,1.023.862-.162.108-1.185.808-.592.431-.808.538-.162.108-.754-.646-.162-.162-1.023-.915.215-.162,1.131-.7.754-.485.646-.377.162-.108.646.538ZM63.47,128.432H81.993l-.538,3.231-1.077,6.3-4.254,2.746H59.27ZM52.808,159.448l5.6-16.316H75.962l3.015,2.639-2.369,13.677Z" transform="translate(-23.03 -48.09)"/>
                                </g>
                            </g>
                        </svg>                              
                    </div>
                    <div class="result-description">
                        <span class="text-result">Seu sistema ocupará uma área de aproximadamente</span>
                        <span id="system-area" class="output-result">63m2</span>
                    </div>                    
                </div>
                <div class="result-card">
                    <div class="svg-wrapper">
                        <svg class="energy-economy" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 85.113 78.247">
                            <g id="if_seo3-42_417759" transform="translate(-4.394 -10.2)">
                                <path class="cls-1" d="M79.318,61.675a5.788,5.788,0,0,1-2.646-.858l.787-1.931a4.4,4.4,0,0,0,2.5.858c1.287,0,1.86-.429,1.86-1.287a1.675,1.675,0,0,0-.5-1.216,5.472,5.472,0,0,0-1.86-1.216A8.628,8.628,0,0,1,77.6,54.88a2.2,2.2,0,0,1-.715-1.073,4.182,4.182,0,0,1-.286-1.359,3,3,0,0,1,.715-2,3.58,3.58,0,0,1,1.86-1.144V47.8h2v1.43a4.55,4.55,0,0,1,2.289.715l-.644,1.86a3.892,3.892,0,0,0-2.36-.787,1.51,1.51,0,0,0-1.144.429,1.3,1.3,0,0,0-.429,1c0,.715.787,1.43,2.289,2.146a12.548,12.548,0,0,1,1.716,1.073,3,3,0,0,1,.787,1.144,3.317,3.317,0,0,1,.286,1.5,2.874,2.874,0,0,1-.715,2,4.023,4.023,0,0,1-2.074,1.216v1.86h-1.86V61.675Z" transform="translate(-20.565 -10.709)"/>
                                <path class="cls-1" d="M74.865,37.761a14.3,14.3,0,1,1-14.3,14.3,14.346,14.346,0,0,1,14.3-14.3m0-2.861A17.165,17.165,0,1,0,92.029,52.065,17.123,17.123,0,0,0,74.865,34.9Z" transform="translate(-15.182 -7.035)"/>
                                <path class="cls-1" d="M77.143,44.33A10.013,10.013,0,1,1,67.13,54.343,10.042,10.042,0,0,1,77.143,44.33m0-1.43A11.443,11.443,0,1,0,88.586,54.343,11.477,11.477,0,0,0,77.143,42.9Z" transform="translate(-17.46 -9.313)"/>
                                <path class="cls-1" d="M46.218,29.075a5.788,5.788,0,0,1-2.646-.858l.787-1.931a4.4,4.4,0,0,0,2.5.858c1.287,0,1.86-.429,1.86-1.287a1.675,1.675,0,0,0-.5-1.216,5.472,5.472,0,0,0-1.86-1.216A8.628,8.628,0,0,1,44.5,22.28a2.2,2.2,0,0,1-.715-1.073,4.182,4.182,0,0,1-.286-1.359,3,3,0,0,1,.715-2,3.58,3.58,0,0,1,1.86-1.144V15.2h2.074v1.43a4.55,4.55,0,0,1,2.289.715l-.644,1.86a3.892,3.892,0,0,0-2.36-.787,1.51,1.51,0,0,0-1.144.429,1.3,1.3,0,0,0-.429,1c0,.715.787,1.43,2.289,2.146a12.547,12.547,0,0,1,1.716,1.073,3,3,0,0,1,.787,1.144,3.317,3.317,0,0,1,.286,1.5,2.874,2.874,0,0,1-.715,2,4.023,4.023,0,0,1-2.074,1.216v1.86h-1.86V29.075Z" transform="translate(-11.138 -1.424)"/>
                                <path class="cls-1" d="M44.043,11.63A10.013,10.013,0,1,1,34.03,21.643,10.042,10.042,0,0,1,44.043,11.63m0-1.43A11.443,11.443,0,1,0,55.486,21.643,11.477,11.477,0,0,0,44.043,10.2Z" transform="translate(-8.033)"/>
                                <path class="cls-1" d="M19.2,94.593a.787.787,0,0,1,.429-.715.71.71,0,0,1,.93.358l4.291,9.512a.7.7,0,1,1-1.287.572l-4.291-9.512A.263.263,0,0,1,19.2,94.593Z" transform="translate(-4.217 -23.816)"/>
                                <path class="cls-1" d="M83.57,83.347a13.965,13.965,0,0,0-1.645.143c-5.292.93-20.6,6.151-23.6,7.152a19.452,19.452,0,0,1-.715,3.29l1.144-.358c.215-.072,18.094-6.222,23.673-7.223a7.132,7.132,0,0,1,1.144-.143,3.83,3.83,0,0,1,1.716.358c-8.868,5.579-25.532,15.877-29.251,17.379a13.4,13.4,0,0,1-4.72,1.144,11.619,11.619,0,0,1-4.434-1.144C43.3,102.515,25.5,96.006,24.853,95.72l-3.147,6.365-7.724,3.29H13.7L7.331,91.93c0-.143.072-.358.143-.358,0,0,17.522-7.08,19.668-7.867a15.351,15.351,0,0,1,5.722-1.073,23.285,23.285,0,0,1,11.443,3.433c4.577,2.575,7.581,4.434,9.369,5.579a4.549,4.549,0,0,1-.5,1.5c-2.5-.215-9.655-2.289-14.3-3.719l-.787,2.646s12.23,3.934,15.591,3.934h.286c1.645-.143,2.432-3.5,2.789-5.65-1.86-1.216-6.151-4.148-11.014-6.866A26.685,26.685,0,0,0,32.863,79.7a18.237,18.237,0,0,0-6.794,1.287c-2.146.858-19.811,7.939-19.811,7.939A3.455,3.455,0,0,0,4.4,92.144,1.9,1.9,0,0,0,4.613,93l6.365,13.589a3.179,3.179,0,0,0,4.005,1.43l7.724-3.29c.787-.358,1.216-1.073,1.86-2.074l1.5-3.361s16.092,5.865,19.668,7.295a14.152,14.152,0,0,0,5.435,1.359,15.032,15.032,0,0,0,5.793-1.359c5.364-2.146,32.541-19.382,32.541-19.382A6.228,6.228,0,0,0,83.57,83.347Z" transform="translate(0 -19.794)"/>
                            </g>
                        </svg>        
                    </div>
                    <div class="result-description">
                        <span class="text-result">Você poderá economizar até</span>
                        <span id="averange-economy" class="output-result">89%</span>
                        <span class="text-result">de sua conta de luz antiga</span>
                    </div>                    
                </div>
            </div>
            <div class="resultado-resumo-wrapper">
                <h3 class="resultado-resumo">Você pode economizar até <span id="economia-valor">R$3.700,00</span> em sua conta de luz.</h3>
                <p><b>Apenas no primeiro ano de operação do sistema.</b></p>
            </div>
        </section>
        <!--footer form-->
        <footer>
           <div class="footer-head">
               <h2><b>Pronto para economizar e gerar sua energia?</b></h2>
               <h2>Solicite um orçamento personalizado.</h2>
               
           </div>
           <form id="form-orcamento-personalizado" class="form-type-2">
                <div class="row">
                    <div class="form-item form-item-float-label col-50">
                        <input name="nome" id="nome" type="text" class="form-text" required autocomplete="off" >
                        <label for="nome" class="form-label">Nome</label>
                    </div>
                    <div class="form-item col-25">
                        <select name="tipo-pessoa" id="tipo-pessoa" class="form-select">
                            <option value="1" selected>Pessoa Física</option>
                            <option value="2">Pessoa Jurídica</option>
                        </select>
                    </div>
                    <div class="form-item form-item-float-label col-25">
                        <input name="email" id="email" type="email" class="form-text" required autocomplete="off" >
                        <label for="email" class="form-label">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-item form-item-float-label col-25">
                        <input name="cpf-cnpj" id="cpf-cnpj" type="text" class="form-text cpf" required autocomplete="off" data-parsley-minlength="14" data-parsley-cpf >
                        <label for="cpf-cnpj" class="form-label">CPF</label>
                    </div>
                    <div class="form-item form-item-float-label col-25">
                        <input name="telefone" id="telefone" type="text" class="form-text phone" required autocomplete="off" >
                        <label for="telefone" class="form-label">Telefone</label>
                    </div>
                    <div class="form-item form-item-float-label col-33">
                        <input name="rua" id="rua" type="text" class="form-text" required autocomplete="off" >
                        <label for="rua" class="form-label">Rua</label>
                    </div>
                    <div class="form-item form-item-float-label col-16">
                        <input name="numero" id="numero" type="text" class="form-text" required autocomplete="off" data-mask="#" >
                        <label for="numero" class="form-label">Número</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-item form-item-float-label col-25">
                        <input name="cep" id="cep" type="text" class="form-text cep" required autocomplete="off" >
                        <label for="cep" class="form-label">CEP</label>
                    </div>
                    <div class="form-item form-item-float-label col-33">
                        <input name="complemento" id="complemento" type="text" class="form-text" autocomplete="off" >
                        <label for="complemento" class="form-label">Complemento</label>
                    </div>
                    <div class="form-item form-item-float-label col-16">
                        <select name="estado" id="estado" class="form-select" required autocomplete="off">
                            <option disabled val=""></option>
                        </select>
                        <label for="estado" class="form-label">Estado</label>
                    </div>
                    <div class="form-item form-item-float-label col-25">
                        <select name="cidade" id="cidade" class="form-select" required autocomplete="off">
                            <option disabled val=""></option>
                        </select>
                        <label for="cidade" class="form-label">Cidade</label>
                        <input type='hidden' name="distribuidora" id="lista_distribuidoras" class="distribuidora_cidade" data-parsley-trigger="change">
                    </div>
                </div>
                <div class="row">
                    <div class="form-item form-item-float-label col-33">
                        <input name="valor-mensal" id="valor-mensal" type="text" class="form-text money" required >
                           <label for="valor-mensal" class="form-label">Valor mensal de energia</label>
                           <div class="radio-content"> 
                               <input type="radio" name="unidade" id="unidade-rs" value="R$" checked="checked">
                               <label for="unidade-rs">R$</label> 
                               <input type="radio" name="unidade" id="unidade-kwh" value="kWh">
                               <label for="unidade-kwh">kWh</label>
                           </div>
                    </div>
                    <div class="btn-wrapper"><button type="submit" class="btn" id="btnEnviaForm">Enviar formulário</button></div>
                </div>
           </form>
        </footer>
        <!--testimonials-->
        <section class="testimonials">
            <div class="testimonials-cards">
                <div class="testimonial-card">
                    <div class="vertical-center">
                        <div class="testimonial-description">
                            <p class="text-testimonial">“Aneel aprova reajuste de tarifa de energia para o interior de São Paulo” </p>
                        </div>  
                        <div class="img-testimonial">
                            <img src="assets/images/files/estadao-logo.png">
                        </div>
                    </div>               
                </div>
                <div class="testimonial-card">
                    <div class="vertical-center">
                        <div class="testimonial-description">
                            <p class="text-testimonial">“Reajuste médio das tarifas de energia na distribuição será de 10%” </p>
                        </div>  
                        <div class="img-testimonial valor-img">
                            <img src="assets/images/files/economioa-valor-logo.png">
                        </div>
                    </div>               
                </div>
                <div class="testimonial-card">
                    <div class="vertical-center">
                        <div class="testimonial-description">
                            <p class="text-testimonial">“Aneel aprova reajustes de 5% a 22,5% em tarifas de distribuidoras de energia” </p>
                        </div>  
                        <div class="img-testimonial">
                            <img src="assets/images/files/folha-logo.png">
                        </div>
                    </div>               
                </div>
            </div>
            <div class="logo-wrapper">
                <img src="assets/images/files/logo-gray.png" />
            </div>
        </section>
        <div class="large reveal" id="modalConfirmacao" data-reveal data-animation-in="fade-in fast" data-animation-out="fade-out fast">
            <h2 class="status">Solicitação enviada com sucesso!<br>Em breve entraremos em contato.</h2>
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

    <!-- JS -->
    <script src='assets/scripts/main.js'></script>
</body>

</html>
