$(window).load(function () {
    //v1
    $("#owl-solutions-slider").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 600,
        paginationSpeed: 700,
        singleItem: true
    });
    //v2
    $(".slider-investimento-solar").owlCarousel({
        loop:true,
        stagePadding: 100,
        center:true,
        items:3,
        margin:20,
        nav:true,
        dots:false,
        navText: ["<i class='fa fa-chevron-left' aria-hidden='true'></i>","<i class='fa fa-chevron-right' aria-hidden='true'></i>"],
       //responsiveClass:true,
        responsive:{
            0: {
                stagePadding:0,
                margin:20,
                autoWidth:true,
                items:1
            },
            1340: {
                stagePadding:100,
                margin:20,
                autoWidth:true
            }
        }
    });
});
