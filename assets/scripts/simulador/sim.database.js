function db_carregaDadosArquivoCSV(sArquivoCSV, bDebug) {
    // Carrega o arquivo de modo sincrono
    jQuery.ajaxSetup({
        async: false
    });

    $.get(sArquivoCSV, function(respons) {
        sCSV = respons;
    });
    // Converte CSV para JSON
    var data = Papa.parse(sCSV, {
        delimiter: ";", // auto-detect
        header: true,
        encoding: 'UTF-8',
        dynamicTyping: true,
    });
    // Volta o jquery para modo assincrono
    jQuery.ajaxSetup({
        async: true
    });
    // Monta a tabela no taffy
    if (bDebug) {
        alert(JSON.stringify(data.data));
    }
    return TAFFY(data.data);

}

//Concessioaria
function db_retornaConcessionariaDaCidade(sCidade, sUF) {
    result = tableDadosCidades({
        municipio: sCidade,
        uf: sUF
    });
  //  alert(result.first());
    r = result.first();
    return r.concessionaria;
}

//Tarifa concessionaria
function db_retornaTarifasConcessionaria(sNomeConcessionaria) {
    result = tableDadosDistribuidores({
        concessionaria: sNomeConcessionaria
    });
    return result.first();
}

//icms
function db_retornaICMS(sUF) {
    result = tableICMS({
        uf: sUF
    });
    r = result.first();
    return r.aliquota;
}

//dados das cidades
function db_retornaDadosCidade(sCidadeEstado, sConcessionaria) {
    result = tableDadosCidades({
        municipio: sCidadeEstado,
        concessionaria: sConcessionaria
    });
    return result.first();
}
