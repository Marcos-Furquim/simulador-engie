Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
window.Parsley
    .addValidator('cpf', { 
        fn : function ( val, req) {
            val = $.trim(val);

            val = val.replace('.','');
            val = val.replace('.','');
            cpf = val.replace('-','');
            while(cpf.length < 11) cpf = "0"+ cpf;
            var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

            var result = true;
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) result = false;

            return result;
        },
        messages: {
            en: 'CPF inválido',
        },
        priority: 32
    });
window.Parsley
    .addValidator('cnpj', function (value, requirement) {
        var   cnpj        = value.replace(/[^0-9]/g, '')
            , len         = cnpj.length - 2
            , numbers     = cnpj.substring(0,len)
            , digits      = cnpj.substring(len)
            , add         = 0
            , pos         = len - 7
            , invalidCNPJ = [
                '00000000000000',
                '11111111111111',
                '22222222222222',
                '33333333333333',
                '44444444444444',
                '55555555555555',
                '66666666666666',
                '77777777777777',
                '88888888888888',
                '99999999999999'
            ]
            , result
            ;


        if ( cnpj.length < 11 || $.inArray(cnpj, invalidCNPJ) !== -1 ) {
            return false;
        }

        for (i = len; i >= 1; i--) {
            add = add + parseInt(numbers.charAt(len - i)) * pos--;
            if (pos < 2) { pos = 9; }
        }

        result = (add % 11) < 2 ? 0 : 11 - (add % 11);
        if (result != digits.charAt(0)) {
            return false;
        }

        len = len + 1;
        numbers = cnpj.substring(0,len);
        add = 0;
        pos = len - 7;

        for (i = 13; i >= 1; i--) {
            add = add + parseInt(numbers.charAt(len - i)) * pos--;
            if (pos < 2) { pos = 9; }
        }

        result = (add % 11) < 2 ? 0 : 11 - (add % 11);
        if (result != digits.charAt(1)) {
            return false;
        }

        return true;

}, 32)
.addMessage('en', 'cnpj', 'Este campo deve ser um CNPJ válido.');
function appendOptionToSelect(selectElem, optionData) {
    var newOption = $(document.createElement('option'));
    newOption.val(optionData.value);
    newOption.html(optionData.html);
    selectElem.append(newOption);
}

function getCidadesDoEstado(siglaEstado) {
    var estado = _.find(estados, { "sigla": siglaEstado });
    return estado.cidades;
}

function isArrayEqual(arr1, arr2) {
    if (!arr1 || !arr2) return false;

    if (arr1.length !== arr2.length) return false;

    for (var i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i]) return false;
    }

    return true;
}

function getCurrentCities(cityElem) {
    var options = cityElem.find("option:not(:disabled)");
    var cities = [];
    _.forEach(options, function (o) {
        cities.push(o.value);
    });
    return cities;
}

// FUNCAO DE SIMULACAO
function simulacao() {
    var estado = $("#simulador-estado").val();
    var cidade = $("#simulador-cidade").val();
    var concessionaria = $('#lista_distribuidoras').val();
    var unidade = $('input[name="simulador-unidade"]:checked').val();
    var valor = $("#simulador-valor-mensal").val() + '';
    valor = valor.replace('.', '');
    valor = valor.replace(',', '.');
    valor = parseFloat(valor);
    debug = true;
    if (debug) {
        if (!console) {
            debug = false;
        }
    }

    // ################# DADOS BASE ###############################
    fValorOriginal = valor;

    if (debug) console.log(estado + '  ' + cidade);

    //Valor tarifa final e bruta
    oTarifasConcessionaria = db_retornaTarifasConcessionaria(concessionaria);
    fTarifaBruta = oTarifasConcessionaria.tarifab;
    fTarifaFinal = oTarifasConcessionaria.tarifaf;

    //ICMS estado
    fIcms = db_retornaICMS(estado);

    // Dados da cidade
    oDadosCidade = db_retornaDadosCidade(cidade, concessionaria)
    fIrradiacao = oDadosCidade.irradiacao;
    fPayback = oDadosCidade.payback;
    sClassificacao = oDadosCidade.classificacao;

    // ============= CALCULOS ======================================
    // CONSUMO EM kWh
    if (unidade == 'kWh') { //não funciona
        fConsumoklw = valor - 100;
    } else {
        fConsumoklw = (valor / fTarifaFinal) - 100;
    }
    fConsumoklw = (valor / fTarifaFinal) - 100;
    if (debug) console.log('Consumo em kWh: ' + fConsumoklw);

    //Sistema indicado
    fIndicadoparte1 = (fConsumoklw * 12) / (((fIrradiacao * 73) / 100) * 365);
    // Arredondamento como a funcao FLOOR do excel
    fSistemaIndicado = 0.265 * Math.floor(fIndicadoparte1 / 0.265);
    if (debug) console.log('Sistemna indicado: ' + fSistemaIndicado);

    //Geracao mensal kWh
    fGeracaoMensal = (fSistemaIndicado * ((fIrradiacao * 73) / 100) * 365) / 12;
    if (debug) console.log('Geracao mensal: ' + fGeracaoMensal);

    //Numero de modulos
    iNmodulos = fSistemaIndicado / 0.265;
    if (debug) console.log('Numero de Modulos: ' + iNmodulos);

    //Area em metros quadrados (sistema)
    fArea = iNmodulos * 1.1 * 1.65 * 0.991;
    if (debug) console.log('Area: ' + fArea);
    //Area em metros quadrados (mostrar no site)
    iAreaSite = Math.round(fArea)
    if (debug) console.log('Area site: ' + iAreaSite);

    //Consumo Antigo
    fConsumoAntigo = (fConsumoklw + 100) * fTarifaFinal;
    if (debug) console.log('Consumo antigo: ' + fConsumoAntigo);

    //Consumo Novo
    fConumoNovo = fConsumoAntigo - (fGeracaoMensal * fTarifaFinal);
    if (debug) console.log('Consumo Novo: ' + fConumoNovo);

    //Economia 1 ano (sistema)
    fEconomia1ano = fGeracaoMensal * 12 * fTarifaFinal;
    if (debug) console.log('Economia 1 ano: ' + fEconomia1ano);
    //Economia 1 ano (mostrar no site)
    // Arredondamento como a funcao FLOOR do excel
    fEconomia1anoSite = 100 * Math.floor(fEconomia1ano / 100);
    if (debug) console.log('Economia 1 ano SITE: ' + fEconomia1anoSite);

    //Economia media % (sistema)
    fEconomiaMedia = (1 - (fConumoNovo / fConsumoAntigo)) * 100;
    if (debug) console.log('Economia media ' + fEconomiaMedia + '%');
    //Economia media % (mostrar no site)
    fEconomiaMediaSite = Math.round(fEconomiaMedia)
    if (debug) console.log('Economia media site ' + fEconomiaMediaSite + '%');


    // Calcula porcentagem de preenchimento do donut chart, baseado no valor de 100% definido no layout por "stroke-dasharray"
    //valor total
    var fValorTotalCirculo = $('#active').attr('stroke-dasharray');
    // valor de preenchimento
    fValorPreenchimento = (fValorTotalCirculo * fEconomiaMediaSite) / 100;

    //avaliacao das cidades
    //substitui valores
    var sClass = '';
    switch (sClassificacao) {
        case 'Regular':
            sClass = 'bad';
            break;
        case 'Boa':
            sClass = 'medium';
            break;
        case 'Muito Boa':
            sClass = 'good';
            break;
        case 'Excelente':
            sClass = 'perfect';
            break;
    }


    // ============= RESULTADOS =====================================

    $("#loading").hide();
    if (iNmodulos <= 0) {
        $('.result').hide();
        $('#result-error').show();
    } else {
        saida = {
            economia_primeiro_ano: fEconomia1anoSite,
            classificacao_cidade: sClassificacao,
            area_sistema: iAreaSite,
            economia_media: fEconomiaMediaSite,
            preenchimento: fValorPreenchimento,
            classe: sClass
        };
        // Mostra div resultados
        $('#result-error').hide();
        $('.result').show();
        setBackgroundHeightPositionEvent();
        $('html, body').animate({
            scrollTop: $('.result').offset().top -50
          }, 800, function(){
            setTimeout(function() {
                    $('#economia-valor').html('R$ '+ saida.economia_primeiro_ano.formatMoney(2, ',', '.'));
                    $('#city-classification').html(saida.classificacao_cidade);
                    $('#system-area').html(saida.area_sistema + 'm²');
                    $('#averange-economy').html(saida.economia_media + '%');
                    //animacoes
                    $(".result-card").attr('class','result-card ' + 'show');
                    $(".resultado-resumo-wrapper").addClass('show');
                    $(".group-arrow-point").attr('class','group-arrow-point ' + saida.classe);
            }, 50);
        });
    }
}
var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
      if (!uniqueId) {
        uniqueId = "Don't call this twice without a uniqueId";
      }
      if (timers[uniqueId]) {
        clearTimeout (timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout(callback, ms);
    };
})();
var setBackgroundHeightPositionEvent = function() {
    var setBackgroundHeightPosition = function() {
        var resultHeight = $(".result").outerHeight();
       // $(".global.simulator-dot").css('background-position-y',resultHeight);
    }
    $(window).resize(function () {
        waitForFinalEvent(function(){
          setBackgroundHeightPosition();
        }, 500, "some unique string");
    });
    setBackgroundHeightPosition();
}
var formSimulatorSettings = function () {
   // Para o IE 9
   $.ajaxSetup({
    cache: false
    });

    //variaveis
    var $selectDeEstados = $("#simulador-estado");
    var $selectDeCidades = $("#simulador-cidade");
    var $valorMensal = $("#simulador-valor-mensal");
    var $btnSimular = $("#btnSimular");


    // Popula os select de Estado
    _.forEach(estados, function (estado) {
        appendOptionToSelect($selectDeEstados, {
            "value": estado.sigla,
            "html": estado.nome
        });
    });
    $selectDeEstados.val("");

    /* Informações processadas no inicio */
    tableICMS = null;
    tableDadosCidades = null;
    tableDadosDistribuidores = null;

    // ICMS nos Estados
    tableICMS = db_carregaDadosArquivoCSV('assets/scripts/simulador/data/icms.csv',false);
    // Irradiação solar
    tableDadosCidades = db_carregaDadosArquivoCSV('assets/scripts/simulador/data/dados_cidades.csv',false);
    // Tarifas das concessionarias
    tableDadosDistribuidores = db_carregaDadosArquivoCSV('assets/scripts/simulador/data/dados_distribuidoras.csv',false);

    // Restabelece o assincronismo do jquery
    jQuery.ajaxSetup({
        async: true
    });


    //eventos
    $selectDeEstados.on("change", function () {
        var siglaEstadoSelecionado = $selectDeEstados.find("option:selected").val();
        var cidades = getCidadesDoEstado(siglaEstadoSelecionado);
        cidades.sort(function (a, b) {
        return a.localeCompare(b)
        });

        $selectDeCidades.find("option").remove(":not(:disabled)");
        
        _.forEach(cidades, function (c) {
            appendOptionToSelect($selectDeCidades, {
                    "value": c,
                    "html": c
                });
            });
        $(this).parent().addClass('form-item-has-value');
        $selectDeCidades.val("").parent().removeClass('form-item-has-value');
    
    });

    $selectDeCidades.on('change',function() {
        $(this).parent().addClass('form-item-has-value');
        var cidadeSelecionada = $(this).find(":selected").val();
        var estado = $selectDeEstados.find(":selected").val();
        var concessionaria = db_retornaConcessionariaDaCidade(cidadeSelecionada,estado);
        $('.distribuidora_cidade').val(concessionaria); 
    });

    $valorMensal.on('keyup input',function() {
        ($(this).val().length > 0) ? $(this).parent().addClass('form-item-has-value') : $(this).parent().removeClass('form-item-has-value') ;
    });

    $btnSimular.off('click').on("click", function(e) {
        if ($('#simulator-form').parsley().isValid()){
            resetResult();
            $("#loading").show();
            setTimeout(function() {
                simulacao();
            }, 1400);
        }
    });

    //validate
    $('#simulator-form').parsley().on('field:validated', function () {
        var ok = $('.parsley-error').length === 0;
        if(ok) {
         
        }
    })
    .on('form:submit', function () {
        return false;
    });
}
var formContact = function () {
   // Para o IE 9
   $.ajaxSetup({
        cache: false
    });

    //variaveis
    $form = $("#form-orcamento-personalizado");
    var $nome = $("#nome");
    var $tipoPessoa = $("#tipo-pessoa");
    var $email = $("#email");
    var $cpfCnpj = $("#cpf-cnpj");
    var $telefone = $("#telefone");
    var $rua = $("#rua");
    var $cep = $("#cep");
    var $numero = $("#numero");
    var $selectDeEstados = $("#estado");
    var $selectDeCidades = $("#cidade");
    var $btnEnviar = $("#btnEnviaForm");
    var $modal = $("#modalConfirmacao");
    var $status = $(".status");


    // Popula os select de Estado
    _.forEach(estados, function (estado) {
        appendOptionToSelect($selectDeEstados, {
            "value": estado.sigla,
            "html": estado.nome
        });
    });
    $selectDeEstados.val("");

    // Restabelece o assincronismo do jquery
    jQuery.ajaxSetup({
        async: true
    });


    //eventos
    $selectDeEstados.on("change", function () {
        var siglaEstadoSelecionado = $selectDeEstados.find("option:selected").val();
        var cidades = getCidadesDoEstado(siglaEstadoSelecionado);
        cidades.sort(function (a, b) {
        return a.localeCompare(b)
        });

        $selectDeCidades.find("option").remove(":not(:disabled)");
        
        _.forEach(cidades, function (c) {
            appendOptionToSelect($selectDeCidades, {
                    "value": c,
                    "html": c
                });
            });
        $(this).parent().addClass('form-item-has-value');
        $selectDeCidades.val("").parent().removeClass('form-item-has-value');
    
    });

    $selectDeCidades.on('change',function() {
        $(this).parent().addClass('form-item-has-value');
        var cidadeSelecionada = $(this).find(":selected").val();
        var estado = $selectDeEstados.find(":selected").val();
        var concessionaria = db_retornaConcessionariaDaCidade(cidadeSelecionada,estado);
        $('.distribuidora_cidade').val(concessionaria); 
    });
    $form.find('.form-text').on('keyup input',function() {
         ($(this).val().length > 0) ? $(this).parent().addClass('form-item-has-value') : $(this).parent().removeClass('form-item-has-value');
    }); 
    $tipoPessoa.change(function () {
        if ($cpfCnpj.hasClass("cpf") && $(this).val() == '2') { //CNPJ
            $cpfCnpj.removeClass('cpf').addClass('cnpj').removeAttr('data-parsley-cpf').attr('data-parsley-cnpj','').siblings('label').text('CNPJ');
        } else { //CPF
            $cpfCnpj.removeClass('cnpj').addClass('cpf').removeAttr('data-parsley-cnpj').attr('data-parsley-cpf','').siblings('label').text('CEP');
        }
        $cpfCnpj.parsley().validate();
    });

    var read_cookie = function (name) {
        var cookies = document.cookie.split(';'),
            d,
            cookie;
        name = name + '=';
        for (d = 0; d < cookies.length; d++) {
            cookie = cookies[d];
            while (cookie.charAt(0) === ' ') { cookie = cookie.substring(1, cookie.length); }
            if (cookie.indexOf(name) === 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return null;
    }
    $btnEnviar.off('click').on("click", function(e) {
        var rd_station_integration = function(doneMessage) {
            var utmz = read_cookie("__utmz");
            var dados = [
                { name: "nome", value: $nome.val() },
                { name: "tipo-pessoa", value: $tipoPessoa.find("option:selected").text() },
                { name: "email", value: $email.val() },
                { name: "cpf-cnpj", value: $cpfCnpj.val() },
                { name: "telefone", value:  $telefone.val() },
                { name: "longradouro", value: $rua.val() },
                { name: "numero", value: $numero.val() },
                { name: "cep", value: $cep.val() },
                { name: "complemento", value: ($("#complemento").val().length)? $("#complemento").val(): 'Sem complemento' },
                { name: "estado", value: $selectDeEstados.find("option:selected").text() },
                { name: "cidade", value: $selectDeCidades.find("option:selected").text() },
                { name: "identificador", value: 'Simulador Engie Solar - Solicitação Orcamentária' },
                { name: "token_rdstation", value: "0e7b2899643b801e6d58a06ebb36ead2" },
                { name: "c_utmz", value: (utmz) ? utmz : '' }

            ];
            console.log(dados);
            $.ajax({
                url: "https://www.rdstation.com.br/api/1.2/conversions",
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: dados,
                dataType:'json',
                crossDomain: true,
                beforeSend:function() {
                    $btnEnviar.attr('disabled','disabled').text('Enviando...');
                }
            }).done(function(resp) {
                // console.log(resp);
                // $form.trigger('reset');
                // $form.find('.form-item').removeClass('form-item-has-value').removeClass('.form-item-valid');
                // $form.find('.form-success-icon').remove();
                // $status.html(doneMessage);
            }).fail(function(){
                //$status.html('<span style="color:#ce4646">Erro ao enviar formulário</span>');
            });
            setTimeout(function() {
                $form.trigger('reset');
                $form.find('.form-item').removeClass('form-item-has-value').removeClass('.form-item-valid');
                $form.find('.form-success-icon').remove();
                $selectDeEstados.val('');
                $selectDeCidades.val('');
                $status.html(doneMessage);
                $btnEnviar.removeAttr('disabled').text('Enviar formulário');
                $modal.foundation('open');
            },1000);
        }
        if ($form.parsley().isValid()){
            rd_station_integration('Solicitação enviada com sucesso!<br>Em breve entraremos em contato.');
        }
    });

    //validate
    $form.parsley().on('field:validated', function () {
        var ok = $('.parsley-error').length === 0;
        if(ok) {
         
        }
    })
    .on('form:submit', function () {
        return false;
    });

}
var resetResult = function() {
    $('.result, #result-error').removeAttr('style');
    $('.group-arrow-point').attr('class', 'group-arrow-point');
    $(".result-card").attr('class','result-card');
    $(".resultado-resumo-wrapper").removeClass('show');
}
var scrollify = function() {
    $.scrollify({
        section : ".scroll",
        updateHash : false,
        before : function () {
            $(".slider-wrapper").removeAttr('style');
        },
        afterRender: function() {
            console.log('afterrender');
           setTimeout(function(){$('html, body').scrollTop(0)},200);
           $.scrollify.disable();
        },
        after : function(index,section) {
            if(index >= 1) {  
                $.scrollify.disable();
            } else {
               // $.scrollify.enable();
            }
        }
      });
}
$(window).load(function() {
    $(document).foundation();
    formSimulatorSettings();
    formContact();
    scrollify();
    
});