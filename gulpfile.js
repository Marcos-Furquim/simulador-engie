'use strict';

var gulp = require('gulp');
//var gutil = require('gulp-util');
var bower = require('gulp-bower');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cache = require('gulp-cache');
var debug = require('gulp-debug');


var config = {
    bowerDir: "./bower_components"
}

function swallowError(error) {
    console.log(error.toString());
    this.emit('end');
}

gulp.task('bower', function () {
    return bower()
        .pipe(gulp.dest(config.bowerDir));
});

gulp.task('sass', function () {
    return gulp.src('assets/scss/**/*.scss')
        //.pipe(debug())
        .pipe(sass())
       // .pipe(debug())
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            remove: false
        }))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('sass:watch', function () {
    gulp.watch('assets/scss/**/*.scss', ['sass']);
});

gulp.task('scripts:watch', function () {
    gulp.watch('assets/scripts/simulador/*.js', ['scripts']);
});

gulp.task('scripts', function () {
    var stripDebug = require('gulp-strip-debug');
    var concat = require('gulp-concat');
    var uglify = require('gulp-uglify');

    var files = [
        'assets/scripts/vendor/jquery.js',
        'assets/scripts/vendor/foundation.js',
        'assets/scripts/vendor/parsley.js',
        'assets/scripts/vendor/lodash.js',
        'assets/scripts/vendor/jquery.mask.js',
        'assets/scripts/vendor/form-mask-setup.js',
        'assets/scripts/vendor/owl.carousel2.min.js',
        'assets/scripts/vendor/owl.carousel-config.js',
        'assets/scripts/vendor/jquery.scrollify.js',
        'assets/scripts/simulador/estados.js',
        'assets/scripts/simulador/taffy.js',
        'assets/scripts/simulador/papaparse.js',
        'assets/scripts/simulador/sim.database.js',
        'assets/scripts/simulador/simulador.js',
    ];
  
    return gulp.src(files)
      .pipe(concat('main.js'))
      .pipe(stripDebug())
      .pipe(uglify())
      .pipe(gulp.dest('assets/scripts/'));
});

gulp.task('scripts-debug', function () {
    var concat = require('gulp-concat');

    var files = [
        'assets/scripts/vendor/jquery.js',
        'assets/scripts/vendor/foundation.js',
        'assets/scripts/vendor/parsley.js',
        'assets/scripts/vendor/lodash.js',
        'assets/scripts/vendor/jquery.mask.js',
        'assets/scripts/vendor/form-mask-setup.js',
        'assets/scripts/vendor/owl.carousel2.min.js',
        'assets/scripts/vendor/owl.carousel-config.js',
        'assets/scripts/vendor/jquery.scrollify.js',
        'assets/scripts/simulador/estados.js',
        'assets/scripts/simulador/taffy.js',
        'assets/scripts/simulador/papaparse.js',
        'assets/scripts/simulador/sim.database.js',
        'assets/scripts/simulador/simulador.js',
    ];
  
    return gulp.src(files)
      .pipe(concat('main.js'))
      .pipe(gulp.dest('assets/scripts/'));
});

gulp.task('default', ['sass:watch', 'scripts:watch']);